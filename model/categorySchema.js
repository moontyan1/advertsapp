const mongoose = require('mongoose')
const Schema = mongoose.Schema

const categoryModel = new Schema({
    title : String
})

module.exports = mongoose.model('Category', categoryModel, 'categories')