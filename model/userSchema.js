const mongoose = require('mongoose')
const Schema = mongoose.Schema

const userModel = new Schema({
    email : { type : String, required : true, index : { unique : true } },
    username : { type : String, required : true, index : { unique : true } },
    password : { type : String, required: true },
    role : { type : String, required: true},
    photoPath : { type: String },
    isBlocked: { type : Boolean, required: true },
    firstName: { type : String },
    lastName: { type : String },
    bio: { type : String },
    contacts: { type : String }
})

module.exports = mongoose.model('User', userModel, 'users')