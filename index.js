const Hapi = require('@hapi/hapi')
const Joi = require('@hapi/joi');
const Boom = require('boom')
const { genSalt, hash } = require('bcrypt')
const { sign } = require('jsonwebtoken')
const { uuid } = require('uuidv4');
const fs = require('fs')
const userModel = require('./model/userSchema')
const categoryModel = require('./model/categorySchema')
const validate = require('./utils/validate')
const validateJWT = require('./utils/jwtValidation')

const mongoose = require('mongoose');
mongoose.connect('mongodb://localhost:27017/advertsApp', { useNewUrlParser: true, useUnifiedTopology: true })
    .then(() => {
        console.log('MongoDB is connected')
    })
    .catch((err) => {
        console.error(err)
    })
const adSchema = new mongoose.Schema({
    title: { type: String, required: true },
    body: { type: String, required: true },
    user: { type: String, required: true },
    category: { type: String, required: true },
    comments: Array,
    created: Date,
    updated: Date
})

adSchema.index({ '$**': 'text' })
const Ad = mongoose.model('Ad', adSchema, "adverts")

const init = async () => {
    const server = new Hapi.Server({
        port: 3000,
        host: 'localhost',
        routes: {
            cors: {
                origin: ['*'],
                credentials: true
            }
        }
    })

    await server.register(require('hapi-auth-jwt2'))
    await server.register(require('@hapi/basic'))
    server.auth.strategy('simple', 'basic', { validate })
    server.auth.strategy('jwt', 'jwt', {
        key: 'secretkey',
        validate: validateJWT
    })
    await server.register(require('inert'))

    server.route({
        method: 'GET',
        path: '/',
        handler: (request, h) => {
            return h.file('./public/index.html')
        }
    })

    server.route({
        method: 'GET',
        path: '/create',
        handler: (request, h) => {
            return h.file('./public/advertForm.html')
        }
    })

    await server.register(require('vision'))

    server.views({
        engines: {
            html: require('handlebars')
        },
        path: __dirname + '/views'
    })

    server.route({
        path: "/public/{path*}",
        method: "GET",
        handler: {
            directory: {
                path: "./public",
                listing: false,
                index: false
            }
        }
    });


    server.route({
        method: 'GET',
        path: '/adverts',
        handler: async (request, h) => {
            const { page = 1, limit = 5 } = request.query
            try {
                const ads = await Ad.find().limit(limit * 1).skip((page - 1) * limit).lean().sort({created: -1})
                const count = await Ad.countDocuments()
                console.log(ads)
                return h.response({
                    ads,
                    totalPages: Math.ceil(count / limit),
                    currentPage: Number(page)
                })
            } catch (error) {
                return h.response(error).code(500)
            }

        }
    })

    server.route({
        method: 'GET',
        path: '/category',
        handler: async (request, h) => {
            try {
                const categories = await categoryModel.find().lean()
                return h.response({ categories })
            } catch (error) {
                return h.response(error).code(500)
            }
        }
    })

    server.route({
        method: 'GET',
        path: '/adverts/category/{category}',
        handler: async (request, h) => {
            try {
                const { page = 1, limit = 5 } = request.query
                const category = request.params.category.charAt(0).toUpperCase() + request.params.category.slice(1)
                const adverts = await Ad.find({ category }).limit(limit * 1).skip((page - 1) * limit).sort({created: -1})
                const count = await Ad.find({ category }).countDocuments()
                return h.response({
                    adverts,
                    totalPages: Math.ceil(count / limit),
                    currentPage: Number(page)
                })
            } catch (error) {
                h.response(error).code(500)
            }
        }
    })

    server.route({
        method: 'GET',
        path: '/adverts/author/{username}',
        handler: async (request, h) => {
            try {
                const username = request.params.username
                const adverts = await Ad.find({ user: username }).sort({created: -1})
                return h.response({ adverts })
            } catch (error) {
                h.response(error).code(500)
            }
        }
    })

    server.route({
        method: 'GET',
        path: '/search',
        handler: async (request, h) => {
            try {
                const { page = 1, limit = 5 } = request.query
                const searchParams = request.query
                console.log(searchParams.advert)
                const result = await Ad.find({ $text: { $search: searchParams.advert } }).limit(limit * 1).skip((page - 1) * limit).lean().sort({created: -1})
                const count = await Ad.find({ $text: { $search: searchParams.advert } }).countDocuments()
                return h.response({
                    result,
                    totalPages: Math.ceil(count / limit),
                    currentPage: Number(page)
                })
            } catch (error) {
                h.response(error).code(500)
            }
        }
    })

    server.route({
        method: 'GET',
        path: '/upload/{image}',
        handler: async (request, h) => {
            return h.file(`assets/images/profilePictures/${request.params.image}`)
        }
    })

    server.route({
        method: 'GET',
        path: '/profilePicture',
        handler: async (request, h) => {
            try {
                const username = request.query.username
                const user = await userModel.findOne({ username }).lean()
                if (!user.photoPath) {
                    const result = {
                        photoPath: 'http://localhost:3000/upload/avatar.png'
                    }
                    return h.response({ result })
                }
                const result = { photoPath: `http://localhost:3000/upload/${user.photoPath}` }
                return h.response({ result })
            } catch (error) {
                return h.response(error).code(500)
            }
        }
    })

    server.route({
        method: 'PUT',
        path: '/upload',
        options: {
            payload: {
                maxBytes: 209715200,
                output: 'stream',
                multipart: true,
                parse: true
            },
            auth: 'jwt'
        },
        handler: async (request, h) => {
            const userId = request.auth.credentials.id
            let data = request.payload
            if (data.file) {
                let defaultName = data.file.hapi.filename
                let filetype = defaultName.split('.').pop()
                let name = `${Date.now()}.${filetype}`
                let path = __dirname + "/assets/images/profilePictures/" + name
                let previousPhoto = await userModel.findOne({ _id: userId })
                if (previousPhoto.photoPath) {
                    let path = __dirname + '/assets/images/profilePictures/' + previousPhoto.photoPath
                    fs.unlinkSync(path)
                }

                let file = fs.createWriteStream(path)

                file.on('error', (err) => console.log(err))

                data.file.pipe(file)

                const user = await userModel.findOneAndUpdate({ _id: userId }, { photoPath: `${name}` }, { new: true })
                let result = {
                    photoURL: user.photoPath
                }

                return h.response({ result }).code(201)
            }
        }
    })

    server.route({
        method: 'POST',
        path: '/adverts',
        handler: async (request, h) => {
            try {
                const { title, body, category } = request.payload
                let user = request.auth.credentials.name
                console.log(user)
                let newAdvert = new Ad({ title, body, user, category, created: Date.now() })
                console.log(newAdvert)
                let result = await newAdvert.save()
                return h.response(result)
            } catch (error) {
                return h.response(error).code(500)
            }
        },
        options: {
            validate: {
                payload: Joi.object({
                    title: Joi.string().min(3).max(50).required(),
                    body: Joi.string().min(5).required().required(),
                    category: Joi.string().required().required()
                })
            },
            payload: {
                parse: true,
                multipart: true
            },
            auth: {
                strategy: 'jwt'
            }
        },
    })

    server.route({
        method: 'POST',
        path: '/register',
        options: {
            payload: {
                parse: true,
                multipart: true
            },
            validate: {
                payload: Joi.object({
                    email: Joi.string().email().required(),
                    username: Joi.string().min(3).max(18).required(),
                    password: Joi.string().min(5).max(25).required()
                })
            }
        },
        handler: async (request, h) => {
            try {
                let { email, username, password } = request.payload
                const salt = await genSalt()
                const hashedPassword = await hash(password, salt)
                const user = new userModel({ email: email, username: username, password: hashedPassword, role: 'user', isBlocked: false })
                let result = await user.save()
                const newUser = { userCredentials: { id: user._id, username: user.username, role: user.role }, accessToken: sign({ id: user._id, username: user.username, role: user.role }, 'secretkey') }
                return h.response({ newUser }).code(201)
            } catch (error) {
                return h.response(error).code(500)
            }
        },
    })

    server.route({
        method: 'POST',
        path: '/login',
        handler: async (request, h) => {
            let userCredentials = request.auth.credentials
            console.log(userCredentials)
            const user = { userCredentials, accessToken: sign({ ...userCredentials }, 'secretkey') }
            return h.response({ user })
        },
        options: {
            auth: {
                strategy: 'simple'
            }
        }
    })

    server.route({
        method: 'DELETE',
        path: '/adverts/{id}',
        handler: async (request, h) => {
            try {
                const advert = await Ad.findOne({ _id: request.params.id }).lean()
                if (advert.user === request.auth.credentials.name || request.auth.credentials.role === 'admin') {
                    let result = await Ad.findOneAndRemove({ _id: request.params.id })
                    return h.response(result)
                }
            } catch (error) {
                return h.response(error).code(500)
            }
        },
        options: {
            auth: {
                strategy: 'jwt'
            }
        }
    })

    server.route({
        method: 'PUT',
        path: '/adverts/{id}',
        options: {
            payload: {
                parse: true,
                multipart: true
            },
            validate: {
                payload: Joi.object({
                    title: Joi.string().min(3).max(50).required(),
                    body: Joi.string().min(5).required().required(),
                    category: Joi.string().required().required()
                })
            },
            auth: {
                strategy: 'jwt'
            }
        },
        handler: async (request, h) => {
            try {
                const { title, body, category } = request.payload
                const user = request.auth.credentials.name
                const advert = await Ad.findOne({ _id: request.params.id }).lean()
                if (advert.user === request.auth.credentials.name) {
                    let result = await Ad.findOneAndUpdate({ _id: request.params.id }, { title, body, user, category, updated: Date.now() }, { new: true })
                    return h.response(result)
                }
            } catch (error) {
                return h.response(error).code(500)
            }
        }
    })

    server.route({
        method: 'PUT',
        path: '/comment',
        options: {
            payload: {
                parse: true,
                multipart: true
            },
            auth: {
                strategy: 'jwt'
            }
        },
        handler: async (request, h) => {
            try {
                const user = request.auth.credentials.name
                let result = await Ad.findOneAndUpdate({ _id: request.payload.id }, { $push: { 'comments': { id: uuid(), user: user, body: request.payload.body } } }, { new: true })
                console.log(result)
                return h.response({ result })
            } catch (error) {
                return h.response(error).code(500)
            }
        }
    })

    server.route({
        method: 'PUT',
        path: '/delete/{commentId}',
        options: {
            payload: {
                multipart: true,
                parse: true
            },
            auth: {
                strategy: 'jwt'
            }
        },
        handler: async (request, h) => {
            try {
                const user = request.auth.credentials.name
                let result = await Ad.findOneAndUpdate({ _id: request.payload.advertId }, { $pull: { 'comments': { id: request.params.commentId, user: user } } }, { new: true })
                console.log(result)
                return h.response(result)

            } catch (error) {
                return h.response(error).code(500)
            }
        }
    })

    server.route({
        method: 'PUT',
        path: '/ban',
        handler: async (request, h) => {
            try {
                const role = request.auth.credentials.role
                const user = request.payload.user
                if (role === 'admin') {
                    const result = await userModel.findOneAndUpdate({ username: user }, { isBlocked: true }, { new: true })
                    const deletedAdverts = await Ad.deleteMany({ user })
                    return { bannedUser: result, deletedAdverts }
                }
                throw Boom.forbidden('You have no rights to do this!')
            } catch (error) {
                return h.response(error).code(500)
            }
        },
        options: {
            payload: {
                parse: true,
                multipart: true
            },
            auth: {
                strategy: 'jwt'
            }
        }
    })

    server.route({
        method: 'GET',
        path: '/adverts/{id}',
        handler: async (request, h) => {
            try {
                let result = await Ad.findById({ _id: request.params.id }).lean().exec()
                console.log(result)
                return h.response({ result })
            } catch (error) {
                return h.response(error).code(500)
            }
        },
    })

    server.route({
        method: 'PUT',
        path: '/user',
        options: {
            payload: {
                parse: true,
                multipart: true
            },
            auth: {
                strategy: 'jwt'
            }
        },
        handler: async (request, h) => {
            try {
                const { firstName, lastName, bio, contacts } = request.payload
                const user = request.auth.credentials.name
                const result = await userModel.findOneAndUpdate({ username: user }, { firstName, lastName, bio, contacts }, { new: true })
                return h.response({
                    firstName: result.firstName,
                    lastName: result.lastName,
                    bio: result.bio,
                    contacts: result.contacts
                })
            } catch (error) {
                h.response(error).code(500)
            }
        }
    })

    server.route({
        method: 'GET',
        path: '/user/{username}',
        handler: async (request, h) => {
            try {
                const result = await userModel.findOne({ username : request.params.username }).lean()
                return h.response({
                    firstName: result.firstName,
                    lastName: result.lastName,
                    bio: result.bio,
                    contacts: result.contacts
                })
            } catch (error) {
                return h.response(error).code(500)
            }
        }
    })

    await server.start()
    console.log('Server is started on ', server.info.uri)
}

process.on('unhandledRejection', (err) => {
    console.log(err)
    process.exit(1)
})

init()
