const userModel = require('../model/userSchema')
const { compare } = require('bcrypt')
const Boom = require('boom')

const validate = async (request, email, password) => {
    const user = await userModel.findOne({ email : email }).lean()
    if (!user) {
        return { credentials: null, isValid: false }
    } else if (user.isBlocked === true) {
        throw Boom.forbidden('This account is banned!')
    }

    const isValid = await compare(password, user.password)
    if (!isValid) {
        throw Boom.unauthorized('Ivalid email or password')
    }
    const credentials = {id: user._id, name: user.username, role: user.role}

    return { isValid, credentials }
}

module.exports = validate