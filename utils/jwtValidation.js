const { AuthCredentials } = require('@hapi/hapi')
const userModel = require('../model/userSchema')


const validateJWT = async (decoded, request, h) => {
    const user = await userModel.findOne({_id : decoded.id})
    if (!user) {
        return { isValid: false }
    }
    const userCredentials = {id: decoded.id, username: decoded.name, role: decoded.role }

    return { isValid: true, userCredentials }
}

module.exports = validateJWT